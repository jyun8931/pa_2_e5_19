/**
 * Project: Assignemt 2 second problem
 * Created by: John Yun
 * Created: 9/15
 * Class: CS 49J
 */

import java.util.Scanner;

public class E5_19 {
    public static void main(String[] args) {
        String strInput;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter the card notation: ");
        strInput = keyboard.nextLine();
        Card hand = new Card(strInput);
        hand.getDescription();

    }
}

/**
 * Sample Output:
 *
 * Enter the card notation: KS
 * King of Spades
 * Process finished with exit code 0
 */