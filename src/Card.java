public class Card {
    private String strNotation;

    //constructor
    public Card(String strNotation){
        this.strNotation = strNotation;
    }
    //getter methods
    public String getStrNotation(){return strNotation;}
    public void setStrNotation(String strTemp) {strNotation = strTemp;}

    //Implementation methods
    public void getDescription(){
        String strDescriptionName = "";
        String strSuite = "";
        if(strNotation.compareTo("9") > 0) {
            switch (strNotation.substring(0, 1)) {
                case "A" -> strDescriptionName = "Ace";
                case "J" -> strDescriptionName = "Jack";
                case "Q" -> strDescriptionName = "Queen";
                case "K" -> strDescriptionName = "King";
            }
        }
        else{
            strDescriptionName = strNotation.substring(0,1);
        }
        switch(strNotation.substring(1)) {
            case "D":
                strSuite = "Diamonds";
                break;
            case "H":
                strSuite = "Hearts";
                break;
            case "C":
                strSuite = "Clubs";
                break;
            case "S":
                strSuite = "Spades";
        }
        System.out.printf("%s of %s", strDescriptionName, strSuite);

    }
}
